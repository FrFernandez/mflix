from app.schema import Schema
from pymongo import MongoClient, DESCENDING, ASCENDING
from pymongo.errors import DuplicateKeyError, OperationFailure, \
    InvalidOperation
from bson.objectid import ObjectId
from bson.errors import InvalidId

import sys

class Database(Schema):
    def __init__(self, collection, schema, config):
        super(Database, self).__init__()
        self.collectionName = collection
        self.collection = None
        self.schema = schema
        self.db = None
        self.get_db(config)


    def get_db(self, config):
        """
        Get instance the database of mongodb.

        :param config:
        :return:
        """
        MFLIX_DB_URI = config['MFLIX_DB_URI']

        if self.db is None:
            self.db = MongoClient(
                MFLIX_DB_URI,
                maxPoolSize=50,
                w=1,
                wtimeout=2500
            )['mflix']
            self.collection = self.db[self.collectionName]


    def find_by_id(self, id, project=None):
        """
        Find by Id document.

        :param id: ObjectId
        :param project: Dict
        :return: document: Dict
        """
        try:

            if not isinstance(project, dict) and project != None:
                raise ValueError("Argument project is not dictionary")

            response = self.collection.find_one({"_id": ObjectId(id)}, project)
            return response

        except InvalidId as e:
            print("Error: Invalid Id", e, file=sys.stdout)
            return {"error": "Invalid Id"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def find_one(self, query=None, project=None):
        """

        :param query: Dict: {
                field: <filter>
            } = filter find one
        :param project: Dict: {
                field: (true|false), (1|0),
                field: "$field"
            } = project find one
        :return:
        """
        try:

            if not isinstance(query, dict) and query is not None:
                raise ValueError("Argument query is not dictionary")
            if not isinstance(project, dict) and project is not None:
                raise ValueError("Argument project is not dictionary")

            response = self.collection.find_one(query, project)
            return response

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def find(self, query=None, project=None, sort=None, skip=0, limit=15):
        """
        Find documents.

        :param query: Dict: {
                field: <filter>
            } = filter find
        :param project: Dict: {
                field: (true|false), (1|0),
                field: "$field"
            } = project find
        :param sort: List: [("field", DESCENDING or ASCENDING)]
        :param skip: int default 0
        :param limit: int default 15
        :return: documents, count
        """
        try:

            if not isinstance(query, dict) and query != None:
                raise ValueError("Ärgument query is not dictionary")
            if not isinstance(project, dict) and project != None:
                raise ValueError("Argument project is not dictionary")
            if not isinstance(sort, list) and sort != None:
                raise ValueError("Argument sort is not list")

            cursor = self.collection.find(query, project)
            count = self.collection.count_documents(query if query != None else {})

            if sort and len(list(sort)) >= 1:
                cursor.sort(sort)
            if skip:
                cursor.skip(skip)

            response = cursor.limit(limit)
            return list(response), count

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def insert_one(self, document):
        """
        Insert one document.

        :param document: List:  document to insert
        :return: inserted_id, acknowledged
        """
        try:

            if not isinstance(document, dict):
                raise ValueError("Argument document is not dictionary")

            response = self.collection.insert_one(document)
            return response.inserted_id, response.acknowledged

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Argument pass is invalid"}

        except DuplicateKeyError as e:
            print("Error: Document already exists", e, file=sys.stdout)
            return {"error": "Document already exists"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def insert_many(self, documents):
        """
        Insert many documents.

        :param documents: List: documents to insert
        :return: inserted_ids, acknowledged, count inserted_ids
        """
        try:

            if not isinstance(documents, list):
                raise ValueError("Argument documents is not list")

            response = self.collection.insert_many(documents)
            return response.inserted_ids, response.acknowledged, len(response.inserted_ids)

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except DuplicateKeyError as e:
            print("Error: Document already exists", e, file=sys.stdout)
            return {"error": "Document already exists"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def replace_one(self, query, replacement, upsert=False):
        """
        Replace one document.

        :param query: Dict: {
                field: <filter>
            } = filter find and replace
        :param replacement: Dict: document a to replace
        :param upsert: bool: if it's true, the document will be inserted if it does not exist,
            otherwise it will be updated
        :return: upserted_id, matched count, modified count, acknowledged
        """
        try:

            if not isinstance(query, dict):
                raise ValueError("Argument query is not dictionary")
            if not isinstance(replacement, dict):
                raise ValueError("Argument replacement is not dictionary")
            if not isinstance(upsert, bool):
                raise  ValueError("Argument upsert is not bool")

            response = self.collection.replace_one(query, replacement, upsert=upsert)
            return response.upserted_id, response.matched_count, response.modified_count, response.acknowledged

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def delete_by_id(self, id):
        """
        Delete by id document.

        :param id: identificator from document
        :return: return id, acknowledged
        """
        try:

            id = ObjectId(id)

            response = self.collection.delete_one({"_id": id})
            return id, response.acknowledged

        except InvalidId as e:
            print("Invalid Id", e, file=sys.stdout)
            return {"error": "Invalid Id"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def delete_one(self, query):
        """
        Delete one document

        :param query: Dict: {
                field: <filter>
            } = filter find and delete
        :return: acknowledged
        """
        try:

            if not isinstance(query, dict):
                raise ValueError("Argument query is not dictionary")

            response = self.collection.delete_one(query)
            return response.acknowledged

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}


    def delete_many(self, query):
        """
        Delete many documents.

        :param query: Dict: {
                field: <filter>
            }
        :return: deleted count, acknowledged
        """
        try:

            if not isinstance(query, dict):
                raise ValueError("Argument query is not dictionary")

            response = self.collection.delete_many(query)
            return response.deleted_count, response.acknowledged

        except (TypeError, ValueError) as e:
            print("Got bad value", e, file=sys.stdout)
            return {"error": "Arguments pass is invalid"}

        except InvalidOperation as e:
            print("Error: Invalid Operation", e, file=sys.stdout)
            return {"error": "Invalid Operation"}

        except OperationFailure as e:
            print("Error: Operation Failure", e, file=sys.stdout)
            return {"error": "Operation Failure"}
