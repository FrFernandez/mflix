from app.main import create_app

import os
import configparser

config = configparser.ConfigParser()
config.read(os.path.abspath(os.path.join('.ini')))

if __name__ == "__main__":
    app = create_app()
    app.config['DEBUG'] = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.config['MFLIX_DB_URI'] = config['TEST']['MFLIX_DB_URI']
    app.config['SECRET_KEY'] = config['TEST']['SECRET_KEY']

    app.run()
