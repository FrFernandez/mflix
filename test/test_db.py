import pytest
from app.db import Database


def get_db(config):
    database = Database("test", None, config)
    return database


@pytest.mark.test_db
@pytest.mark.usefixtures('app')
def test_db(app):
    database = get_db(app.config)
    print(database.db.list_collection_names(), database)


@pytest.mark.test_find_by_id
@pytest.mark.usefixtures('app')
def test_find_by_id(app):
    database = get_db(app.config)
    print(database.find_by_id())


@pytest.mark.test_find_one
@pytest.mark.usefixtures('app')
def test_find_one(app):
    database = get_db(app.config)
    print(database.find_one())